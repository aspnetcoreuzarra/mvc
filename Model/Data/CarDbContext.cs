﻿using Microsoft.EntityFrameworkCore;
using Model.Models;

namespace Model.Data
{
    public class CarDbContext : DbContext
    {
        public CarDbContext(DbContextOptions<CarDbContext> options) : base(options)
        {
            
        }

        public DbSet<Car> Cars { get; set; }
    }
}