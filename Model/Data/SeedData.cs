﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Model.Data
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using var context = new CarDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<CarDbContext>>());

            if (context.Cars.Any()) return;

            var cars = File.ReadLines("App_Data/cars.csv")
                .Skip(2)
                .Where(line => !string.IsNullOrEmpty(line))
                .ToCar()
                .ToList();
            
            // TODO
        }
    }
}