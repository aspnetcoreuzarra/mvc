﻿using System.Collections.Generic;
using Model.Models;

namespace Model.Data
{
    public static class CarExtensions
    {
        public static IEnumerable<Car> ToCar(this IEnumerable<string> source)
        {
            foreach (var item in source)
            {
                var cols = item.Split(";");
                yield return new Car
                {
                    Name = cols[0],
                    //TODO
                };
            }
        }
    }
}